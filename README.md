# S-RPM v4.11
------------------------------------------------
Sun-Red Hat Package Manager is a free package manager for the Sun Operating System and other Arch distrobuton, some of the binaries are owned by Red Hat and others are owned by SunOS Linux
You can build S-RPM on the following OSes:
1. Arch Linux ------> v2020.10 and later
2. Manajaro --------> v19.0.2 and later (depends on spin)
3. Endeavor OS  ----> v2020.09.20 and later
4. Arch Labs -------> v2020.11.04 and later
5. ArchBang --------> v2020.01.11 and later
6. Sun/OS (Linux) --> v2020.12.01 and later
7. All Red Hat distros (2019 and later)

We will continue to add support for more platforms as we continue to release!
Thanks,
Morales Research Corporation

-------------------------------------------------

(C) 2021 Morales Research Corp and SunOS Linux

-------------------------------------------------

(C) 2020 Red Hat Inc

-------------------------------------------------
